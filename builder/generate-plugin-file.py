#!/usr/bin/env python3

from tempfile import NamedTemporaryFile
import hashlib
import json
import os
import os.path as path
import re
import requests
import sys
import xml.etree.ElementTree as ET

def main():
    author = os.environ["PLUGIN_AUTHOR"]
    project_namespace = os.environ["CI_PROJECT_NAMESPACE"]
    project_name = os.environ["CI_PROJECT_NAME"]
    package_list_file = os.environ["SLACKWARE_PACKAGE_LIST_FILE"]
    delete_plugin_path = os.environ.get("DELETE_PLUGIN_PATH", "true") != "false"
    version = os.environ.get("VERSION")
    if version == "" or version == None:
        version = "0000-00-00"
    output_path = os.environ["OUTPUT_PATH"]
    package_path = f"{output_path}/{project_name.replace('-', '_')}-{version}.txz"

    plugin_name_regex = re.compile("^unraid-plugin-(?P<name>.+)$")
    plugin_name = plugin_name_regex.match(project_name).group("name")

    plugin_path = f"/boot/config/plugins/{plugin_name}"
    plugin_manifest_filename = f"{project_name}.plg"

    releases_url = f"https://gitlab.com/{project_namespace}/{project_name}/-/releases"

    if os.path.isfile(package_list_file):
        with open(package_list_file, "r") as package_list:
            slackware_packages = json.load(package_list)
    else:
        slackware_packages = []

    plugin = ET.Element("PLUGIN", {
        "name": plugin_name,
        "version": version,
        "author": author,
        "pluginURL": f"{releases_url}/permalink/latest/downloads/{plugin_manifest_filename}",
    })

    for initial_config in iterate_initial_config_files(plugin_path):
        add_inline_file_element(plugin, initial_config["content"], destination=initial_config["destination"])

    packages = [generate_slackware_package_info(p) for p in slackware_packages] + [generate_package_info(package_path, version, releases_url)]

    for package in packages:
        add_package_file_element(plugin, os.path.join(plugin_path, package['file_name']), package['url'], package['md5'])

    for install_hook in iterate_install_hook_files():
        add_inline_file_element(plugin, install_hook, run="on_install")

    for remove_hook in iterate_remove_hook_files():
        add_inline_file_element(plugin, remove_hook, run="on_remove")

    add_inline_file_element(plugin, generate_base_remove_hook(plugin_path, packages, delete_plugin_path), run="on_remove")

    ET.indent(plugin)
    tree = ET.ElementTree(plugin)
    with open(path.join(output_path, plugin_manifest_filename), "wb") as output_file:
        tree.write(output_file, encoding='utf-8', xml_declaration=True)

def add_package_file_element(parent, destination, url, md5):
    package_element = ET.SubElement(parent, "FILE", {
        "Name": destination,
        "Run": "upgradepkg --install-new"
    })
    url_element = ET.SubElement(package_element, "URL")
    url_element.text = url
    md5_element = ET.SubElement(package_element, "MD5")
    md5_element.text = md5

def add_inline_file_element(parent, inline_content, destination=None, run=None):
    file_element = ET.SubElement(parent, "FILE")

    if run == "on_install" or run == "on_remove":
        file_element.set("Run", "/bin/bash")
    if run == "on_remove":
        file_element.set("Method", "remove")
    if destination != None:
        file_element.set("Name", destination)

    inline_element = ET.SubElement(file_element, "INLINE")
    inline_element.text = inline_content

def generate_package_info(package_path, version, releases_url):
    package_file_name = path.basename(package_path)
    package_name = path.splitext(package_file_name)[0]
    package_md5 = md5sum_file(package_path)

    return {
        "name": package_name,
        "file_name": package_file_name,
        "url": f"{releases_url}/{version}/downloads/{package_file_name}",
        "md5": package_md5,
    }

def generate_slackware_package_info(slackware_package):
    package_file_name = f"{slackware_package['name']}-{slackware_package['version']}-x86_64-{slackware_package['revision']}.txz"
    package_name = path.splitext(package_file_name)[0]
    package_url = f"https://mirrors.slackware.com/slackware/slackware64-current/slackware64/{slackware_package['location']}/{package_file_name}"
    package_md5 = md5sum_url(package_url)

    return {
        "name": package_name,
        "file_name": package_file_name,
        "url": package_url,
        "md5": package_md5,
    }

def md5sum_url(url):
    digest = hashlib.md5()
    download = requests.get(url, stream=True)
    for chunk in download.iter_content(chunk_size=512):
        digest.update(chunk)
    return digest.hexdigest()

def md5sum_file(path):
    digest = hashlib.md5()
    with open(path, "rb") as file:
        for chunk in iterate_file(file, 512):
            digest.update(chunk)

    return digest.hexdigest()

def iterate_file(file, chunk_size):
    while True:
        chunk = file.read(chunk_size)
        if len(chunk) > 0:
            yield chunk
        else:
            break

def iterate_initial_config_files(plugin_path):
    initial_configs_directory = 'initial-configs'

    if not path.isdir(initial_configs_directory):
        return

    for subdir, dirs, files in os.walk(initial_configs_directory):
        for file in files:
            source_path = path.join(subdir, file)
            with open(source_path, 'r') as file:
                contents = file.read()
            destination = path.join(plugin_path, path.relpath(source_path, initial_configs_directory))
            yield {
                "content": contents,
                "destination": destination,
            }

def iterate_install_hook_files():
    for hook_file in iterate_hook_files("hooks/install"):
        yield hook_file

def iterate_remove_hook_files():
    for hook_file in iterate_hook_files("hooks/remove"):
        yield hook_file

def iterate_hook_files(hook_directory):
    if not path.isdir(hook_directory):
        return

    hook_paths = [path.join(hook_directory, file_path) for file_path in os.listdir(hook_directory) if path.isfile(path.join(hook_directory, file_path))]

    for file_path in hook_paths:
        with open(file_path, "r") as file:
            contents = file.read()
        yield contents

def generate_base_remove_hook(plugin_path, packages, delete_plugin_path=True):
    script = ""
    for package in reversed(packages):
        script += f"removepkg \"{package['name']}\"\n"
    if delete_plugin_path:
        script += f"rm -rf \"{plugin_path}\""
    return script

main()
