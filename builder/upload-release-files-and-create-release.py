#!/usr/bin/env python3

import json
import os
import requests
import subprocess
import sys

def main():
    output_directory = os.path.abspath(os.environ["OUTPUT_PATH"])
    job_token = os.environ["CI_JOB_TOKEN"]
    version = os.environ['VERSION']
    package_registry_url = f"{os.environ['CI_API_V4_URL']}/projects/{os.environ['CI_PROJECT_ID']}/packages/generic/{os.environ['CI_PROJECT_NAME']}/{version}"
    
    if not os.path.isdir(output_directory):
        print(f"No directory {output_directory}. Failing.")
        os.exit(1)

    files_to_upload = [os.path.join(output_directory, file_path) for file_path in os.listdir(output_directory) if os.path.isfile(os.path.join(output_directory, file_path))]

    manifests = []

    for file_path in files_to_upload:
        print(f"Uploading file {os.path.basename(file_path)}.")
        file_name = os.path.basename(file_path)
        file_url = f"{package_registry_url}/{file_name}"

        with open(file_path, "rb") as file:
            response = requests.put(file_url, data=file, headers={"JOB-TOKEN": job_token})

        if response.status_code != 200 and response.status_code != 201:
            print(f"Failed to upload file {os.path.basename(file_path)} to url {file_url}. Code: {response.status_code}. Text: {response.text}")
            sys.exit(1)

        manifests.append({
            "name": file_name,
            "filepath": f"/{file_name}",
            "url": file_url,
            "link_type": "other",
        })

    release_cli_command = ["release-cli", "create", "--name", version, "--tag-name", version] + generate_asset_link_parameters(manifests)
    print(" ".join(release_cli_command))
    completed_process = subprocess.run(release_cli_command)
    if completed_process.returncode != 0:
        print("Failed to create release.")
        sys.exit(1)

def generate_asset_link_parameters(manifests):
    result = []
    for manifest in manifests:
        result.append("--assets-link")
        result.append(json.dumps(manifest))
    return result

main()
