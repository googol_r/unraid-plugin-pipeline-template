#!/usr/bin/env python3

from tempfile import TemporaryDirectory
import os
import os.path
import shutil
import subprocess
import sys

def main():
    build_root_directory = os.environ["BUILD_ROOT"]
    hooks_directory = "hooks/generate-package-contents"
    static_package_source_directory = "package_source"

    if not os.path.isdir(hooks_directory):
        print(f"No directory {hooks_directory}.")
        hook_paths = []
    else:
        hook_paths = [os.path.abspath(os.path.join(hooks_directory, file_path)) for file_path in os.listdir(hooks_directory) if os.path.isfile(os.path.join(hooks_directory, file_path)) and os.access(os.path.join(hooks_directory, file_path), os.X_OK)]

    with TemporaryDirectory() as collecting_dir:
        shutil.copytree(static_package_source_directory, collecting_dir, dirs_exist_ok=True)

        for hook_path in hook_paths:
            print(f"Running hook {hook_path}")
            with TemporaryDirectory() as working_dir:
                completed_process = subprocess.run([hook_path], cwd=working_dir)
                if completed_process.returncode != 0:
                    print(f"Hook {hook_path} failed.")
                    sys.exit(1)
                shutil.copytree(working_dir, collecting_dir, dirs_exist_ok=True)

        shutil.copytree(collecting_dir, build_root_directory, dirs_exist_ok=True)

main()
